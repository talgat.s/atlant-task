package main

import (
	"github.com/gofiber/fiber"
	"github.com/gofiber/helmet"
	"github.com/gofiber/limiter"
	"github.com/gofiber/logger"
	"github.com/gofiber/recover"
	log "gopkg.in/inconshreveable/log15.v2"
	"net/http"
	"os"

	"gitlab.com/talgat.s/atlant-task/env"
	"gitlab.com/talgat.s/atlant-task/routes"
)

func setupRouter() *fiber.App {
	r := fiber.New(&fiber.Settings{
		Prefork:       true,
		CaseSensitive: true,
		StrictRouting: true,
		ServerHeader:  "Fiber",
	})
	if env.IsNot(env.LOCAL_PROD, env.REMOTE_PROD) {
		r.Use(logger.New())
	}
	if env.Is(env.LOCAL_PROD, env.REMOTE_PROD) {
		r.Use(limiter.New())
	}
	r.Use(helmet.New())
	r.Use(recover.New(
		recover.Config{
			Handler: func(c *fiber.Ctx, err error) {
				c.SendString(err.Error())
				c.SendStatus(http.StatusInternalServerError)
			},
		}),
	)

	routes.Config(r)
	return r
}

func main() {
	// setup env
	env.Config()

	// routes
	app := setupRouter()

	port := os.Getenv("PORT")
	log.Info("Starting APP on localhost", "PORT", port)
	err := app.Listen(port)
	log.Crit("Unable to start web server at", err)
}
