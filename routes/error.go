package routes

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber"
	"net/http"

	"gitlab.com/talgat.s/atlant-task/utils"
)

func errorParser(err error) []fiber.Map {
	switch perr := err.(type) {
	case validator.ValidationErrors:
		errors := []fiber.Map{}
		for _, err := range perr {
			detail := fmt.Sprintf("Field validation for %s failed on the '%s' tag", err.Field(), err.Tag())
			errors = append(errors, fiber.Map{"detail": detail})
		}
		return errors
	// just for a case
	case *validator.InvalidValidationError:
		return []fiber.Map{fiber.Map{"detail": fmt.Sprint("Server error ::", perr.Error())}}
	default:
		return []fiber.Map{fiber.Map{"detail": perr.Error()}}
	}
}

func errors(r *fiber.Group) {
	r.Use("/", func(c *fiber.Ctx) {
		if err := c.Error(); err != nil {
			c.Set("Content-Type", "application/json")
			switch herr := err.(type) {
			case *utils.HttpErr:
				errors := errorParser(herr.Err)
				c.Status(herr.Status).JSON(fiber.Map{"errors": errors})
				return
			default:
				errors := []fiber.Map{fiber.Map{"detail": err.Error()}}
				c.Status(http.StatusInternalServerError).JSON(fiber.Map{"errors": errors})
				return
			}
		}

		c.Next()
	})
}
