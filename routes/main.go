package routes

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber"
	"reflect"
	"strconv"
)

const (
	default_limit = 10

	tagParam = "param"
	tagQuery = "query"
)

type (
	piece struct {
		ID string `param:"id" validate:"required,uuid4"`
	}

	list struct {
		Search string `query:"search" validate:"omitempty"`
		Limit  int    `query:"limit" validate:"omitempty,min=0"`
		Offset int    `query:"offset" validate:"omitempty,min=0"`
	}
)

var validate *validator.Validate

func Config(r *fiber.App) {
	setupValidator()

	api := r.Group("/api")

	// v1
	v1 := api.Group("/v1")
	errors(v1)
}

func setupValidator() {
	validate = validator.New()
}

func decodeBody(c *fiber.Ctx, s interface{}) error {
	if err := c.BodyParser(s); err != nil {
		return err
	}
	if err := validate.Struct(s); err != nil {
		return err
	}
	return nil
}

func decodeParam(c *fiber.Ctx, s interface{}) error {
	if err := parse(c, s, tagParam); err != nil {
		return err
	}
	if err := validate.Struct(s); err != nil {
		return err
	}
	return nil
}

func decodeQuery(c *fiber.Ctx, s interface{}) error {
	if err := parse(c, s, tagQuery); err != nil {
		return err
	}
	if err := validate.Struct(s); err != nil {
		return err
	}
	return nil
}

func parse(c *fiber.Ctx, s interface{}, tagName string) error {
	v := reflect.ValueOf(s)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		return fmt.Errorf("schema: interface must be a pointer to struct")
	}

	e := v.Elem()
	t := e.Type()

	for i := 0; i < t.NumField(); i++ {
		var q string

		f := t.Field(i)
		tag := f.Tag.Get(tagName)

		switch tagName {
		case tagParam:
			q = c.Params(tag)
		case tagQuery:
			q = c.Query(tag)
		}

		switch f.Type.Kind() {
		case reflect.Int:
			val, err := strconv.Atoi(q)
			if err != nil {
				e.Field(i).SetInt(0)
			}
			e.Field(i).SetInt(int64(val))
		case reflect.String:
			e.Field(i).SetString(q)
		default:
			return fmt.Errorf("Type was not implemented yet ::", f.Type.Kind())
		}
	}

	return nil
}
