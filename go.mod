module gitlab.com/talgat.s/atlant-task

go 1.14

require (
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gofiber/fiber v1.9.6
	github.com/gofiber/helmet v0.0.3
	github.com/gofiber/limiter v0.0.6
	github.com/gofiber/logger v0.0.9
	github.com/gofiber/recover v0.0.5
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.10.6 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/inconshreveable/log15.v2 v2.0.0-20200109203555-b30bc20e4fd1
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
