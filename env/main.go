package env

import (
	"github.com/joho/godotenv"
	log "gopkg.in/inconshreveable/log15.v2"
	"os"
)

const (
	APP_ENV = "APP_ENV"

	LOCAL_DEV   = "LOCAL_DEV"
	LOCAL_PROD  = "LOCAL_PROD"
	LOCAL_TEST  = "LOCAL_TEST"
	REMOTE_DEV  = "REMOTE_DEV"
	REMOTE_PROD = "REMOTE_PROD"
	REMOTE_TEST = "REMOTE_TEST"
)

func appEnv() string {
	env := os.Getenv(APP_ENV)
	if env == "" {
		env = LOCAL_DEV
	}
	return env
}

func Config() {
	env := appEnv()
	log.Info("Setup current env", "APP_ENV", env)

	godotenv.Load(".env")
	switch env {
	case LOCAL_DEV:
		godotenv.Load(".env.local.dev")
	case LOCAL_PROD:
		godotenv.Load(".env.local.prod")
	case LOCAL_TEST:
		godotenv.Load(".env.local.test")
	case REMOTE_DEV:
		godotenv.Load(".env.remote.dev")
	case REMOTE_PROD:
		godotenv.Load(".env.remote.prod")
	case REMOTE_TEST:
		godotenv.Load(".env.remote.test")
	}
}

func Is(envs ...string) bool {
	e := appEnv()
	for _, env := range envs {
		if env == e {
			return true
		}
	}
	return false
}

func IsNot(envs ...string) bool {
	e := appEnv()
	for _, env := range envs {
		if env == e {
			return false
		}
	}
	return true
}
