package utils

type HttpErr struct {
	Status int
	Err    error
}

func (err *HttpErr) Error() string {
	return err.Err.Error()
}
